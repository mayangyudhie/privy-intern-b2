# Task Based UI :
1. Focus each page on a single task.
Fokus pada setiap halaman pada satu tugas utama. Sehingga, user tidak perlu melakukan banyak hal.
2. Make your task be like commands. 
Memberikan penjelasan apa saja yang harus dilakukan oleh user.
3. Be mindful of your model.
4. Check your links.
- Untuk meminimalisir link pada page lain yang bukan bagian dari task tersebut.
- Melakukan pelacakan link untuk menyelesaikan task.
5. Follow Established UI Patterns.

# User Flow Diagram :
- Circle : untuk mulai atau akhir.
- Rectangle : step on flow of the chart atau menunjukan langkah aliran proses
- Diamond decision: untuk menunjukkan sebuah langkah  pengambilan keputusan. Umumnya, menggunakan bentuk pertanyaan, dan biasanya jawabannya terdiri dari (Ya atau Tidak) yang menentukan bagaimana alur selanjutnya berjalan berdasarkan kriteria atau pertanyaan tersebut.
- Line : menunjukkan arah aliran dari satu proses ke proses yang lain.
- Dot line: rute alternatif untuk kembali ke layar sebelumnya.